import React, { Component } from "react";
import "./Typer.component.less";

class Typer extends Component {
  state = {
    text: "",
    isDeleting: false,
    loopNum: 0,
    typingSpeed: 150,
  };

  componentDidMount() {
    this.handleType();
  }
  timer1 = null;
  timer2 = null;

  handleType = () => {
    const { dataText } = this.props;
    const { isDeleting, loopNum, text, typingSpeed } = this.state;
    const i = loopNum % dataText.length;
    const fullText = dataText[i];

    this.setState({
      text: isDeleting
        ? fullText.substring(0, text.length - 1)
        : fullText.substring(0, text.length + 1),
      typingSpeed: 150,
    });

    if (!isDeleting && text === fullText) {
      this.timer1 = setTimeout(() => this.setState({ isDeleting: true }), 1000);
    } else if (isDeleting && text === "") {
      this.setState({
        isDeleting: false,
        loopNum: loopNum + 1,
      });
    }

    this.timer2 = setTimeout(this.handleType, typingSpeed);
  };

  componentWillUnmount() {
    clearTimeout(this.timer1);
    clearTimeout(this.timer2);
  }

  render() {
    return (
      <>
        <div className='typer'>
          <div className='row justify-content-center'>
            <div className='col-10'>
              <h1>
                <span>{this.state.text}</span>
                <span id='cursor'></span>
              </h1>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Typer;
