import React from "react";
import "./NavMenu.component.less";
import { Menu } from "antd";
import { FaHome, FaCommentDots, FaImages } from "react-icons/fa";
import { MdLibraryBooks, MdContacts } from "react-icons/md";
const { SubMenu } = Menu;
const NavMenu = () => {
  return (
    <>
      <Menu defaultSelectedKeys={["1"]} mode='inline'>
        <Menu.Item key='1' icon={<FaHome style={{ fontSize: "20px" }} />}>
          <span className='menu-word'>&nbsp;HOME</span>
        </Menu.Item>
        <Menu.Item
          key='2'
          icon={<FaCommentDots style={{ fontSize: "20px" }} />}
        >
          <span className='menu-word'>&nbsp;ABOUT</span>
        </Menu.Item>
        <SubMenu
          key='sub2'
          title={<span className='menu-word'>&nbsp;COURSES</span>}
          icon={<MdLibraryBooks style={{ fontSize: "20px" }} />}
        >
          <Menu.Item key='sub-min-4'>
            <span className='menu-word'>React</span>
          </Menu.Item>
          <Menu.Item key='sub-min-5'>
            <span className='menu-word'>WEB-DESIGN</span>
          </Menu.Item>
          <Menu.Item key='sub-min-6'>
            <span className='menu-word'>PHP FRAMEWORK</span>
          </Menu.Item>
          <Menu.Item key='sub-min-7'>
            <span className='menu-word'>Java</span>
          </Menu.Item>
          <Menu.Item key='sub-min-8'>
            <span className='menu-word'>ANGULAR JS</span>
          </Menu.Item>
          <Menu.Item key='sub-min-9'>
            <span className='menu-word'>REACT NATIVE</span>
          </Menu.Item>
          <Menu.Item key='sub-min-10'>
            <span className='menu-word'>WORDPRESS</span>
          </Menu.Item>
          <Menu.Item key='sub-min-11'>
            <span className='menu-word'>MERN</span>
          </Menu.Item>
        </SubMenu>

        <Menu.Item key='3' icon={<FaImages style={{ fontSize: "20px" }} />}>
          <span className='menu-word'> GALLERY</span>
        </Menu.Item>
        <Menu.Item key='4' icon={<MdContacts style={{ fontSize: "20px" }} />}>
          <span className='menu-word'>&nbsp;CONTACT</span>
        </Menu.Item>
      </Menu>
    </>
  );
};

export default NavMenu;
