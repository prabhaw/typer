import React, { Component } from "react";
import "./SearchInput.component.less";
import { Input } from "antd";

const { Search } = Input;
class SearchInput extends Component {
  render() {
    return (
      <>
        <form className='search-form d-none d-sm-block ml-auto'>
          <Search
            placeholder='input search text'
            onSearch={(value) => console.log(value)}
            className='search-input'
          />
        </form>
      </>
    );
  }
}

export default SearchInput;
