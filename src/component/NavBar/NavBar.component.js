import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Drawer } from "antd";
import "./NavBar.component.less";
import SearchInput from "./SearchInput/SearchInput.component";
import NavMenu from "./NavMenu/NavMenu.component";
import {
  FaBars,
  FaEnvelope,
  FaPhone,
  FaSignInAlt,
  FaTwitter,
  FaFacebookF,
  FaYoutube,
  FaLinkedin,
} from "react-icons/fa";

class NavBar extends Component {
  state = { visible: false, placement: "right" };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };
  render() {
    const { placement, visible } = this.state;
    return (
      <>
        <header id='header' className='header fixed-top'>
          <div className='header-top-wrapper'>
            <div className='container-fluid' style={{ padding: "0 5% 0 5%" }}>
              <div className='row'>
                <div className='ml-auto col-8'>
                  <ul className='d-flex flex-wrap list-unstyled justify-content-end aling-items-center tobar-item'>
                    <li>
                      <Link to='#'>
                        <FaEnvelope />
                        <span>&nbsp;mail.prabhaw@gmail.com</span>
                      </Link>
                    </li>
                    <li>
                      <Link to='#'>
                        <FaPhone />
                        <span>&nbsp;+977-9845697677</span>
                      </Link>
                    </li>

                    <li>
                      <Link to='#'>
                        <FaSignInAlt />
                        <span>&nbsp;LogIn</span>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className='header-bottom-block'>
            <div
              className='container-fluid'
              style={{ padding: "5px 5% 5px 5%" }}
            >
              <div className='header-bottom-wrapper d-flex justify-content-between'>
                <div className='header-logo'>
                  <Link to='/'>
                    <img
                      src={"https://greenqbit.com/images/GreenqbitWide.svg"}
                      alt='GreenQbit Technology'
                      title='GreenQbit Technology'
                      style={{ marginTop: "-30px" }}
                      className='img-fluid'
                    />
                  </Link>
                </div>
                <SearchInput />
                <ul className='tab-item d-flex list-unstyled'>
                  <li>
                    <FaBars
                      style={{ fontSize: "30px", color: "#ff304f" }}
                      onClick={this.showDrawer}
                    />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <Drawer
            title={
              <Link to='/'>
                <img
                  src={"https://greenqbit.com/images/GreenqbitWide.svg"}
                  alt='GreenQbit Technology'
                  title='GreenQbit Technology'
                  style={{ marginTop: "-30px" }}
                  className='img-fluid'
                />
              </Link>
            }
            zIndex={1500}
            placement={placement}
            closable={false}
            onClose={this.onClose}
            visible={visible}
            key={placement}
            className={"nav-drawer"}
          >
            <NavMenu />
            <div className='nav-social-icon' style={{ marginLeft: "24px" }}>
              <h6>Follow Us On:</h6>
              <ul>
                <li>
                  <FaFacebookF className='social-icon facebook' />
                </li>
                <li>
                  <FaTwitter className='social-icon twitter' />
                </li>
                <li>
                  <FaYoutube className='social-icon youtube' />
                </li>
                <li>
                  <FaLinkedin className='social-icon linkedin' />
                </li>
              </ul>
            </div>
          </Drawer>
        </header>
        <div style={{ paddingTop: "80px" }}></div>
      </>
    );
  }
}

export default NavBar;
