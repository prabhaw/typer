import React from "react";
import "./Slider.component.less";

import { Carousel } from "antd";
const Slider = () => {
  const img = [
    `${process.env.PUBLIC_URL}/img/slidepic.JPG`,
    `${process.env.PUBLIC_URL}/img/slidepic2.JPG`,
  ];

  const images = img.map((item, i) => (
    <div key={i}>
      <img src={`${item}`} alt='carousel-img' className='carousel-img' />
      <div className='overlay'>
        <div class='text'>Hello World</div>
      </div>
    </div>
  ));
  return (
    <>
      <Carousel className='carousel' autoplay pauseOnHover={false}>
        {images}
      </Carousel>
    </>
  );
};

export default Slider;
