import React from "react";
import "./App.less";
import "bootstrap/dist/css/bootstrap.min.css";
import Routers from "./Router/Application.routes";
import CKEditor from 'ckeditor4-react';
function App() {
  return (
    <>
      <CKEditor data="<p>This is an example CKEditor 4 WYSIWYG editor instance.</p>" />
    </>
  );
}

export default App;
