import React from "react";
import { Switch, Route } from "react-router-dom";
import HomePage from "./../component/HomePage/HomePage.component";

const Routers = () => {
  return (
    <Switch>
      <Route exact path='/' component={HomePage} />
    </Switch>
  );
};

export default Routers;
